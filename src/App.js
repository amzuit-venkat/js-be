import React from 'react';
import './App.css';
import NavBar from './components/navbar/NavBar';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Home from './components/home/Home';
import NewJob from './components/job/NewJob';
import Login from './components/SignIn';
import Payment from './components/payment/Payment';
import Order from './components/order/Order';
import Messages from "./components/messages/Messages";
import Credit from "./components/credit/Credit";
import Settings from './components/settings/Settings';
import SignUpPage from './components/SignUp';
import SignInPage from './components/SignIn'
import * as ROUTE from './constants/routes';

const login = {
    home: false,
    main: false,
    newJob: false,
    login: false,
    signUp: true,
    payments: false,
    order: false,
    credits: false,
    messages: false,
    settings: false

};
const sign_up = {
    home: false,
    main: false,
    newJob: false,
    login: true,
    signUp: false,
    payments: false,
    order: false,
    credits: false,
    messages: false,
    settings: false

};
const after_login = {
    home: true,
    main: true,
    newJob: true,
    login: false,
    signUp: false,
    payments: true,
    order: true,
    credits: true,
    messages: true,
    settings: true,
    logout: true
};
const LoginContainer = () => (
    <div className="App">
        <NavBar menu={login}/>
        <Route exact path={ROUTE.HOME} render={() => <Redirect to={ROUTE.LOGIN}/>}/>
        <Route path={ROUTE.LOGIN} component={Login}/>
    </div>
);
const SignUpContainer = () => (
    <div className="App">
        <NavBar menu={sign_up}/>
        <Route exact path={ROUTE.HOME} render={() => <Redirect to={ROUTE.LOGIN}/>}/>
        <Route path={ROUTE.SIGN_UP} component={SignUpPage}/>
        <Route path={ROUTE.LOGIN} component={SignInPage}/>
    </div>
);
const DefaultContainer = () => (
      <div className="App">
          <NavBar menu={after_login}/>
          <Route exact path={ROUTE.HOME} component={Home}/>
          <Route exact path={ROUTE.MAIN} component={Home}/>
          <Route path={ROUTE.NEW_JOB} component={NewJob}/>
          <Route path={ROUTE.LOGIN} component={SignInPage}/>
          <Route path={ROUTE.SIGN_UP} component={SignUpPage}/>
          <Route path={ROUTE.PAYMENT} component={Payment}/>
          <Route path={ROUTE.ORDER} component={Order}/>
          <Route path={ROUTE.CREDITS} component={Credit}/>
          <Route path={ROUTE.MESSAGES} component={Messages}/>
          <Route path={ROUTE.SETTINGS} component={Settings}/>
      </div>
)

function App() {
  return (
    <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path={ROUTE.LOGIN} component={LoginContainer}/>
            <Route exact path={ROUTE.SIGN_UP} component={SignUpContainer}/>
            <Route component={DefaultContainer}/>
          </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
