import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { SignUpLink } from '../SignUp';
import { withFirebase } from '../firebase';
import * as ROUTES from '../../constants/routes';
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/es/FormGroup";
import './login.css';
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {FacebookLoginButton, GoogleLoginButton, TwitterLoginButton} from "react-social-login-buttons";
const SignInPage = () => (
    <div>
        <Card className="LoginCard">
            <Card.Title>
                <h2>SignIn</h2>
            </Card.Title>
            <Card.Body>
                <SignInForm />
                <br/>
                <SignInGoogle />
                <SignInTwitter />
                <SignUpLink />
            </Card.Body>

        </Card>
    </div>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email, password } = this.state;

        this.props.firebase
            .doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({ ...INITIAL_STATE });
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const { email, password, error } = this.state;

        const isInvalid = password === '' || email === '';

        return (
            <Form onSubmit={this.onSubmit}>
                <FormGroup controlId="signIn.email">
                    <Form.Label column="false">Email</Form.Label>
                    <Form.Control
                        type="email"
                        value={email}
                        onChange={this.onChange}
                        placeholder = "Email Address"
                    />
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </FormGroup>
                <FormGroup controlId="signIn.password">
                    <Form.Label column="false">Password</Form.Label>
                    <Form.Control
                        type="password"
                        value={password}
                        onChange={this.onChange}
                        placeholder = "Password"
                    />
                </FormGroup>

                <Button disabled={isInvalid} type="submit">
                    Sign In
                </Button>

                {error && <p>{error.message}</p>}
            </Form>
        );
    }
}

class SignInGoogleBase extends Component {
    constructor(props) {
        super(props);

        this.state = { error: null };
    }

    onSubmit = event => {
        this.props.firebase
            .doSignInWithGoogle()
            .then(socialAuthUser => {
                // Create a user in your Firebase Realtime Database too
                this.props.firebase
                    .user(socialAuthUser.user.uid)
                    .set({
                        username: socialAuthUser.user.displayName,
                        email: socialAuthUser.user.email,
                        roles: [],
                    })
                    .then(() => {
                        this.setState({ error: null });
                        this.props.history.push(ROUTES.HOME);
                    })
                    .catch(error => {
                        this.setState({ error });
                    });
            })
            .catch(error => {
                this.setState({ error });
            });

    };

    render() {
        const { error } = this.state;

        return (
            <Form onSubmit={this.onSubmit} className="ButtonForm">
                <GoogleLoginButton onClick={this.onSubmit} type="submit">Sign In with Google</GoogleLoginButton>
                {error && <p>{error.message}</p>}
            </Form>
        );
    }
}

class SignInFacebookBase extends Component {
    constructor(props) {
        super(props);

        this.state = { error: null };
    }

    onSubmit = event => {
        this.props.firebase
            .doSignInWithFacebook()
            .then(socialAuthUser => {
                // Create a user in your Firebase Realtime Database too
                this.props.firebase
                    .user(socialAuthUser.user.uid)
                    .set({
                        username: socialAuthUser.additionalUserInfo.profile.name,
                        email: socialAuthUser.additionalUserInfo.profile.email,
                        roles: [],
                    })
                    .then(() => {
                        this.setState({ error: null });
                        this.props.history.push(ROUTES.HOME);
                    })
                    .catch(error => {
                        this.setState({ error });
                    });
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    render() {
        const { error } = this.state;

        return (
            <Form className="ButtonForm">
                <FacebookLoginButton onClick={this.onSubmit} type="submit">Sign In with Facebook</FacebookLoginButton>

                {error && <p>{error.message}</p>}
            </Form>
        );
    }
}

class SignInTwitterBase extends Component {
    constructor(props) {
        super(props);

        this.state = { error: null };
    }

    onSubmit = event => {
        this.props.firebase
            .doSignInWithTwitter()
            .then(socialAuthUser => {
                // Create a user in your Firebase Realtime Database too
                this.props.firebase
                    .user(socialAuthUser.user.uid)
                    .set({
                        username: socialAuthUser.additionalUserInfo.profile.name,
                        email: socialAuthUser.additionalUserInfo.profile.email,
                        roles: [],
                    })
                    .then(() => {
                        this.setState({ error: null });
                        this.props.history.push(ROUTES.HOME);
                    })
                    .catch(error => {
                        this.setState({ error });
                    });
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    render() {
        const { error } = this.state;

        return (
            <Form className="ButtonForm">
                <TwitterLoginButton onClick={this.onSubmit} type="submit">Sign In with Twitter</TwitterLoginButton>
                {error && <p>{error.message}</p>}
            </Form>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
)(SignInFormBase);

const SignInGoogle = compose(
    withRouter,
    withFirebase,
)(SignInGoogleBase);

const SignInFacebook = compose(
    withRouter,
    withFirebase,
)(SignInFacebookBase);

const SignInTwitter = compose(
    withRouter,
    withFirebase,
)(SignInTwitterBase);

export default SignInPage;

export { SignInForm, SignInGoogle, SignInFacebook, SignInTwitter };
