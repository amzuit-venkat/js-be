import React from 'react';
import {Navbar, Nav, Badge} from 'react-bootstrap';
import './navbar.css';
import * as ROUTES from '../../constants/routes';

class NavBar extends React.Component {


    render() {

        return(
            <div>
                <Navbar sticky="top" bg="light" expand="lg">
                    <Navbar.Brand href={ROUTES.HOME} >Nerdzu BE</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="base-navbar-nav">
                        <Nav className="mr-auto">
                           { this.props.menu.home?<Nav.Link href={ROUTES.HOME}>Home</Nav.Link>:null}
                           { this.props.menu.newJob?<Nav.Link href={ROUTES.NEW_JOB}>New</Nav.Link>:null}
                           {this.props.menu.credits?<Nav.Link href={ROUTES.CREDITS}>Credits <Badge variant="primary">9+3</Badge></Nav.Link>:null}
                            {this.props.menu.messages?<Nav.Link href={ROUTES.MESSAGES}>Messages <Badge variant="success">2</Badge></Nav.Link>:null}
                            {this.props.menu.settings?<Nav.Link href={ROUTES.SETTINGS}>Settings</Nav.Link>:null}
                        </Nav>
                        <Nav>
                            {!this.props.auth?<Nav.Link href={ROUTES.SIGN_UP}>SignUp</Nav.Link>:null}
                            {!this.props.auth?<Nav.Link href={ROUTES.LOGIN}>Login</Nav.Link>:null}
                            {this.props.auth?<Nav.Link href={ROUTES.LOGIN}>Logout</Nav.Link>:null}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

            </div>
        );
    }

}

export default NavBar;
