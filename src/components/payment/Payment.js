import React from 'react';
import { Container, Button, FormGroup, FormControl, FormLabel, Row, Col, Card } from "react-bootstrap";
import './payment.css';
import Form from 'react-bootstrap/FormGroup';


class Payment extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            cardnumber: "",
            expirymonth: "",
            expiryyear: "",
            cvvcode: ""
        }
    }

    render() {
        return(
            <div>
                <Container>
                <Card className="Payment">
                <Form>
                    <Card.Title>Payment</Card.Title>
                    <Card.Body>
                        <Row>
                            <Col xl>
                                <FormGroup controlId="cardnumber" bsSize="large">
                                    <FormLabel>Card Number</FormLabel>
                                    <FormControl
                                        autoFocus
                                        type="text"
                                        value={this.state.cardnumber}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs>
                                <FormGroup controlId="expirymonth" bsSize="small">
                                    <FormLabel>Month</FormLabel>
                                    <FormControl
                                        autoFocus
                                        type="text"
                                        value={this.state.expirymonth}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs>
                                <FormGroup controlId="expiryyear" bsSize="small">
                                    <FormLabel>Year</FormLabel>
                                    <FormControl
                                        autoFocus
                                        type="text"
                                        value={this.state.expiryyear}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs>
                            <FormGroup controlId="cvvcode" bsSize="small">
                                    <FormLabel>CVV</FormLabel>
                                    <FormControl
                                        autoFocus
                                        type="text"
                                        value={this.state.cvvcode}
                                    />
                            </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col xl>
                            <Button className="primary">Pay</Button>
                            </Col>
                        </Row>
                    </Card.Body>
                </Form>
                </Card>
                </Container>
            </div>
        );
    }

}
export default Payment;