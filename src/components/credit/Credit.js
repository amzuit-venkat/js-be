import React from 'react';
import {Card} from 'react-bootstrap';
import Table from "react-bootstrap/Table";
import './credit.css';

class Credit extends React.Component {

    render() {
        return(
            <div className="Credit">
                <Card>
                    <Card.Body>
                        <Table style={{textAlign: 'left'}} >
                            <thead>
                                <th>Product</th>
                                <th>Available</th>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}
export default Credit;
