import React from 'react';
import {Form, Container, Button} from 'react-bootstrap';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6
import { withRouter } from 'react-router-dom';

import './edit.css';
import Home from "../Home";

class Edit extends React.Component {

    state = {
        text: '',
        loadHome: false
    };

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
      }
    
      handleChange(value) {
        this.setState({ text: value })
      }


    render() {
        return(
            <div className="Edit">
               {this.state.loadHome?<Home/>:<Container>
                   <Form>
                       <Form.Group controlId="form.newjob.title" style={{textAlign: 'left'}} >
                           <Form.Label column="false">Title</Form.Label>
                           <Form.Control autoFocus type="text" placeholder="Job Title"/>
                       </Form.Group>
                       <Form.Group controlId="form.newjob.uniqueid" style={{textAlign: 'left'}} >
                           <Form.Label column="false">Unique Job Id</Form.Label>
                           <Form.Control type="text" defaultValue={this.props.uniqueId}/>
                       </Form.Group>
                       <Form.Group controlId="form.newjob.desc" style={{textAlign: 'left'}} >
                           <Form.Label column="false">Description</Form.Label>
                           <ReactQuill theme="snow" value={this.state.text}
                                    onChange={this.handleChange} />
                        </Form.Group>
                       <Form.Group controlId="form.newjob.email" style={{textAlign: 'left'}} >
                           <Form.Label column={false}>Email All Applications To</Form.Label>
                           <Form.Control type="email" placeholder="Email"/>
                       </Form.Group>
                        <Form.Group>
                            <Button onClick={()=> {this.props.backHome()}}>Back</Button>
                        </Form.Group>
                   </Form>
               </Container>}
            </div>
        );
    }
}
export default withRouter(Edit);
