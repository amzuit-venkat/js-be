import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import './home.css';
import Button from "react-bootstrap/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import Edit from "./edit/Edit";
import Modal from "react-bootstrap/Modal";






class Home extends React.Component {

    state = {
        showJob: false,
        uniqueId: null,
        showDelete: false
    };

    jobs = [{
        id: 1,
        uniqueId: "J001",
        title: "Java Developer",
        type: "Contract",
        startDate: "30 Jun 2019",
        endDate: "30 Jul 2019",
        applications: 15,
        status: "Active"
    },
        {
            id: 2,
            uniqueId: "J002",
            title: ".NET Developer",
            type: "Permanant",
            startDate: "14 Jul 2019",
            endDate: "30 Jul 2019",
            applications: 40,
            status: "Ended"
        },
        {
            id: 3,
            uniqueId: "J003",
            title: "DevOps Engineer",
            type: "Contract",
            startDate: "02 Jul 2019",
            endDate: "30 Jul 2019",
            applications: 23,
            status: "Active"
        }
    ];

    columns = [
        {
            dataField: 'id',
            text: 'Id'
        },
        {
            dataField: 'uniqueId',
            text: 'Unique Id',
        },
        {
            dataField: 'title',
            text:'Title'
        },
        {
            dataField: 'type',
            text: 'Type'
        },
        {
            dataField: 'startDate',
            text: 'Start Date'
        },
        {
            dataField: 'endDate',
            text: 'End Date'
        },
        {
            dataField: 'applications',
            text: 'Applications Received'
        },
        {
            dataField: 'status',
            text: 'Status'
        },
        {
            dataField:'Action',
            text:'View',
            formatter : (cell, row, rowIndex, extraData)  => {
                return(<div>
                        <Button onClick={()=> {this.setState({showJob:true, uniqueId: row.uniqueId})}} variant="link"><FontAwesomeIcon icon={faEdit}/></Button>
                        <Button onClick={()=> {this.setState({showDelete: true})}} variant="link"><FontAwesomeIcon icon={faTrash}/></Button>
                    </div>
                );
            }
        }

    ];

    showHome = () => {
        this.setState({showJob: false})
    };

    hideDelete = () => {
        this.setState({showDelete: false})
    };
    render() {
        return(
            <div className="Home">
                {this.state.showJob?<Edit backHome ={this.showHome} uniqueId={this.state.uniqueId}/>:
                    <BootstrapTable keyField="id" data={this.jobs} columns={this.columns} striped hover>
                </BootstrapTable>}
                <Modal show={this.state.showDelete} onHide={() => {this.hideDelete()}}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Would you like to delete this Job? This will just hide the job, you can un-hide and publish anytime you want.
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={() => {this.hideDelete()}}>Delete</Button>
                        <Button variant="secondary" onClick={() => {this.hideDelete()}}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default Home;
