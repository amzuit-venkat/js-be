import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import Button from "react-bootstrap/Button";
import './Messages.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye } from '@fortawesome/free-solid-svg-icons'




var messages = [{
    id: 1,
    title: "Invoice - Gen: 1 + Perm 1",
    date: "01/07/2019",
    attachment: 1,
},
    {
        id: 2,
        title: "Invoice Gen 5 + Prem 2",
        date: "23/06/2019",
        attachment: 1,
    },
    {
        id: 3,
        title: "Invoice Gen 4",
        date: "16/06/2019",
        attachment: 1,
    },
    {
        id: 4,
        title: "Invoice Gen 1",
        date: "14/06/2019",
        attachment: 1,
    },
];

const columns = [
    {
        dataField: 'id',
        text: 'Id'
    },
    {
        dataField: 'title',
        text: 'Title'
    },
    {
        dataField: 'date',
        text: 'Date'
    },
    {
        dataField:'attachment',
        text:'View',
        formatter : (cell) => {
            return(<Button variant="link"
                    > <FontAwesomeIcon icon={faEye} /> </Button>);
        }
    }
];


class Messages extends React.Component {

    render() {
        return(
            <div className="Messages">
                <BootstrapTable keyField="id" data={messages} columns={columns} striped />
            </div>
        );
    }
}
export default Messages;
