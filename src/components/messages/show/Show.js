import React from 'react';
import {Card, Table} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { withRouter } from 'react-router-dom';

class Show extends React.Component {

    render() {
        return(
            <Card>
                <Card.Header>Message {this.props.title}</Card.Header>
                <Card.Text>
                    <Table>
                        <tr>
                            <td>{this.props.message}</td>
                        </tr>
                        <tr>
                            <td>Attachment: <a href={this.props.downloadLink}>{this.props.fileName}</a> </td>
                        </tr>
                    </Table>
                </Card.Text>
                <Card.Footer>
                    <Button variant="primary" onClick={() => this.props.history.goBack()}>Back</Button>
                </Card.Footer>
            </Card>
        );
    }
}
export default withRouter(Show);
