import React from 'react';
import {Form, Container, Button} from 'react-bootstrap';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css'; // ES6

import './newjob.css';

class NewJob extends React.Component {

    constructor(props) {
        super(props)
        this.state = { text: '' } // You can also pass a Quill Delta here
        this.handleChange = this.handleChange.bind(this)
      }
    
      handleChange(value) {
        this.setState({ text: value })
      }
    render() {

        return(
            <div className="NewJob">
               <Container>
                   <Form>
                       <Form.Group controlId="form.newjob.title" style={{textAlign: 'left'}} >
                           <Form.Label column={false}>Title</Form.Label>
                           <Form.Control autoFocus type="text" placeholder="Job Title"/>
                       </Form.Group>
                       <Form.Group controlId="form.newjob.jobid" style={{textAlign: 'left'}} >
                           <Form.Label column={false}>Job Id</Form.Label>
                           <Form.Control type="text" placeholder="Job Id"/>
                       </Form.Group>
                       <Form.Group controlId="form.newjob.desc" style={{textAlign: 'left'}} >
                           <Form.Label column={false}>Description</Form.Label>
                           <ReactQuill theme="snow" value={this.state.text}
                  onChange={this.handleChange} />
                        </Form.Group>
                       <Form.Group controlId="form.newjob.email" style={{textAlign: 'left'}} >
                           <Form.Label column={false}>Email All Applications To</Form.Label>
                           <Form.Control type="email" placeholder="Email"/>
                       </Form.Group>
                       <Form.Group>
                           <Button variant="primary">Save</Button>
                       </Form.Group>
                   </Form>
               </Container>
            </div>
        );
    }
}
export default NewJob;
