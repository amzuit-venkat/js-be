import React from 'react';
import { Table, Button} from 'react-bootstrap';
import './order.css';

class Order extends React.Component {

    render() {
        return(
            <div >
                <br/>
                <Table responsive="sm" className="d-flex flex-row justify-content-center align-items-center" bordered>
                   <tbody>
                       <tr>
                           <td>Super Job Credits:</td>
                           <td>9</td>
                       </tr>
                       <tr>
                           <td>Premium Job Credits:</td>
                           <td>3</td>
                       </tr>
                       <tr>
                           <td colSpan="2">
                               <Button>Add Credit</Button>
                           </td>
                       </tr>
                   </tbody>
                </Table>
            </div>
        );
    }

}
export default Order;
