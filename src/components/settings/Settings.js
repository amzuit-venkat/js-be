import React from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import './settings.css';

class Settings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userData:null
        }
    }

    componentDidMount() {
        this.props.firebase.userData()
            .on('value', snapshot => {
                const userDetails = snapshot.val();
                if(userDetails) {
                    const uData = Object.keys(userDetails).map(key =>({
                        ...userDetails[key],
                        uid: key,
                    }) );
                    this.setState({userData: uData});
                } else {
                    this.setState({userData: null});
                }
            });
    }

    render() {
        const {userData} = this.state;

        return(
            <div className="Settings">
                <Card>
                    <Card.Title><h2>Settings</h2></Card.Title>
                    <Card.Body>
                        <Form>
                        <Form.Group controlId="formName" style={{textAlign: 'left'}}>
                            <Form.Label column={false}>Name</Form.Label>
                            <Form.Control autoFocus type="text" value={userData.name}/>
                        </Form.Group>
                            <Form.Group controlId="forCompanyName" style={{textAlign: 'left'}}>
                                <Form.Label column={false}>Company Name (Displayed in Posting)</Form.Label>
                                <Form.Control type="text" value={userData.compantName}/>
                            </Form.Group>
                        <Form.Group controlId="forJobEmail" style={{textAlign: 'left'}}>
                            <Form.Label column={false}>Email Applications To</Form.Label>
                            <Form.Control type="email" value={userData.emailAppsTo}/>
                        </Form.Group>
                        <Form.Group controlId="forEmail" style={{textAlign: 'left'}}>
                            <Form.Label column={false}>Email</Form.Label>
                            <Form.Control  disabled={true} value={userData.email} type="email"/>
                        </Form.Group>
                        <Form.Group controlId="formPassword" style={{textAlign: 'left'}}>
                            <Form.Label column={false}>Password</Form.Label>
                            <Form.Control type="password" value={userData.password}/>
                        </Form.Group>
                        <Form.Group controlId="formPasswordRetype" style={{textAlign: 'left'}}>
                            <Form.Label column={false}>Retype-Password</Form.Label>
                            <Form.Control type="password" value={userData.password}/>
                        </Form.Group>

                        <Button variant="primary" type="submit">
                            Save
                        </Button>
                        &nbsp;
                        &nbsp;
                        <Button variant="secondary" type="submit">
                            Cancel
                        </Button>
                        </Form>
                    </Card.Body>
                </Card>
                
            </div>
        );
    }

}

export default Settings;
