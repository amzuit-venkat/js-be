import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';

import { withFirebase } from '../firebase';
import * as ROUTES from '../../constants/routes';
import Form from "react-bootstrap/Form";
import {Card, FormGroup} from "react-bootstrap";
import './signup.css';
const SignUpPage = () => (
    <Card className="LoginCard">
        <Card.Header><h2>SignUp</h2></Card.Header>
        <Card.Body>
            <SignUpForm />
        </Card.Body>
    </Card>
);

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

class SignUpFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { username, email, passwordOne } = this.state;

        this.props.firebase
            .doCreateUserWithEmailAndPassword(email, passwordOne)
            .then(authUser => {
                // Create a user in your Firebase realtime database
                this.props.firebase
                    .user(authUser.user.uid)
                    .set({
                        username,
                        email,
                    })
                    .then(() => {
                        this.setState({ ...INITIAL_STATE });
                        this.props.history.push(ROUTES.HOME);
                    })
                    .catch(error => {
                        this.setState({ error });
                    });
            })
            .catch(error => {
                this.setState({ error });
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    onChangeCheckbox = event => {
        this.setState({ [event.target.name]: event.target.checked });
    };

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;

        const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === '';

        return (
            <Form onSubmit={this.onSubmit} className="ButtonForm">
                <FormGroup controlId="signUp.email">
                    <Form.Label column="false">User Name</Form.Label>
                    <Form.Control
                        type="email"
                        value={email}
                        onChange={this.onChange}
                        placeholder = "Email Address"
                    />
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                </FormGroup>
                <FormGroup controlId="signUp.passwordOne">
                    <Form.Label column="false">Password</Form.Label>
                    <Form.Control
                        type="password"
                        value={passwordOne}
                        onChange={this.onChange}
                        placeholder = "Password"
                    />
                </FormGroup>
                <FormGroup controlId="signUp.passwordTwo">
                    <Form.Label column="false">Password</Form.Label>
                    <Form.Control
                        type="password"
                        value={passwordOne}
                        onChange={this.onChange}
                        placeholder = "Password"
                    />
                </FormGroup>
                <button disabled={isInvalid} type="submit">
                    Sign Up
                </button>

                {error && <p>{error.message}</p>}
            </Form>
        );
    }
}

const SignUpLink = () => (
    <p>
        Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
    </p>
);
const SignUpForm = withRouter(withFirebase(SignUpFormBase));
export default SignUpPage;
export { SignUpForm, SignUpLink };
