import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import FirebaseContext from './context';

const devConfig = {
    apiKey: "AIzaSyCB3CKN7ST57UmDnHp2IXMDLp7I5_rPb4o",
    authDomain: "nerdzu-be.firebaseapp.com",
    databaseURL: "https://nerdzu-be.firebaseio.com",
    projectId: "nerdzu-be",
    storageBucket: "",
    messagingSenderId: "1057803291904",
    appId: "1:1057803291904:web:d6701e66983760dc",
    clientId: "30420579002-itbq4vmus23697op863g1hr9g47aqqtf.apps.googleusercontent.com",
    scopes: [
        "email",
        "profile",
        "https://www.googleapis.com/auth/cloud-platform",
        "https://www.googleapis.com/auth/jobs"
    ],
    discoveryDocs: ["https://cloud.google.com/talent-solution/job-search/docs/"]
};

const prodConfig = {
    apiKey: "AIzaSyCB3CKN7ST57UmDnHp2IXMDLp7I5_rPb4o",
    authDomain: "nerdzu-be.firebaseapp.com",
    databaseURL: "https://nerdzu-be.firebaseio.com",
    projectId: "nerdzu-be",
    storageBucket: "",
    messagingSenderId: "1057803291904",
    appId: "1:1057803291904:web:d6701e66983760dc",
    clientId: "30420579002-itbq4vmus23697op863g1hr9g47aqqtf.apps.googleusercontent.com",
    scopes: [
        "email",
        "profile",
        "https://www.googleapis.com/auth/cloud-platform",
        "https://www.googleapis.com/auth/jobs"
    ],
    discoveryDocs: ["https://cloud.google.com/talent-solution/job-search/docs/"]
};

const config =
    process.env.NODE_ENV === 'production' ? prodConfig : devConfig;

class Firebase {
    constructor() {
        app.initializeApp(config);
        this.auth = app.auth();
        this.db = app.database();
        this.googleProvider = new app.auth.GoogleAuthProvider();
        this.facebookProvider = new app.auth.FacebookAuthProvider();
        this.twitterProvider = new app.auth.TwitterAuthProvider();

    }

    // *** Auth API ***

    doCreateUserWithEmailAndPassword = (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignInWithGoogle = () =>
        this.auth.signInWithPopup(this.googleProvider);

    doSignInWithFacebook = () =>
        this.auth.signInWithPopup(this.facebookProvider);

    doSignInWithTwitter = () =>
        this.auth.signInWithPopup(this.twitterProvider);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);

    // *** Merge Auth and DB User API *** //

    onAuthUserListener = (next, fallback) =>
        this.auth.onAuthStateChanged(authUser => {
            if (authUser) {
                this.user(authUser.uid)
                    .once('value')
                    .then(snapshot => {
                        const dbUser = snapshot.val();

                        // default empty roles
                        if (!dbUser.roles) {
                            dbUser.roles = [];
                        }

                        // merge auth and db user
                        authUser = {
                            uid: authUser.uid,
                            email: authUser.email,
                            ...dbUser,
                        };

                        next(authUser);
                    });
            } else {
                fallback();
            }
        });

    // *** User API ***

    user = uid => this.db.ref(`users/${uid}`);

    userData = () => this.db.ref(`users/` + this.auth.currentUser.uid)




}

export default Firebase;
export { FirebaseContext };
